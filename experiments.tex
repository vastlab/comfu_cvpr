\section{Experiments}
\label{sec:experiments}
\begin{table}[t!]
  \centering
  \footnotesize
  \setlength{\tabcolsep}{.4em}
  \begin{tabular}{c|c|c|c}
    \bf Dataset  & \bf \# of Instances & \bf \# of Attributes & \bf \# of Classes \\\hline
    Breast Cancer  & 683 & 9 & 2 \\
    Digits  & 1797 & 64 & 10 \\
    Glass & 214 & 9 & 6 \\
    Ionosphere & 351 & 34 & 2 \\
    Iris  & 150 & 4 & 3 \\
    Seeds  & 210 & 7 & 3 \\
    Wine  & 178 & 13 & 3 \\
    Yeast & 1484 & 8 & 10 \\
  \end{tabular}

  \capt{tab:datasets}{Benchmark Datasets}{The UCI machine learning repository datasets frequently used for cluster fusion evaluation.}
\end{table}


We evaluate our ComFu algorithm on eight different datasets including a wide variety of applications from the UCI machine learning repository on which other researchers have reported clustering and cluster fusion results.
These datasets are Iris flower, Wine, Seeds, Digits, Breast Cancer, Yeast, Glass, and Ionosphere \cite{Dua:2017}, each of which provides a pre-defined feature vector for each contained sample.
Details of the benchmark datasets are given in \tab{datasets}.

\subsection{Evaluation}
\label{sec:evaluation}
To evaluate the quality of the fused clusters, we utilize the normalized mutual information (NMI), which is also used in related work.
NMI includes ground-truth labels, and it is possible to compute the NMI between different clustering results having different numbers of clusters.
We rely on the implementation of this measure in the scikit-learn Python library \cite{pedregosa2011sklearn}.
Let $\hat C$ be the clustering result and $\mathbf C$ the ground-truth; then the NMI is computed as \cite{strehl2002cluster}:
\begin{equation}
  \label{eq:NMI}
  \mathrm{NMI}(\hat C,\mathbf C) = \frac
    {\sum\limits_{i=1}^{\hat N} \sum\limits_{j=1}^{\mathbf N} V_{ij} \log{\frac {V_{ij}V}{\hat V_i \mathbf V_j}}}
    {\sqrt{\sum\limits_{i=1}^{\hat N} \hat V_i \log{\frac {\hat V_i}{V}} + \sum\limits_{j=1}^{\mathbf N}\mathbf V_j\log{\frac {\mathbf V_j}{V}}}}
\end{equation}
where $V$ is the total number of samples, $\hat V_i$ the number of samples in cluster $\hat C_i$, $\mathbf V_j$ the number of samples in ground-truth cluster $\mathbf C_j$, and $V_{ij}$ the number of samples that are in both clusters $\hat C_i$ and $\mathbf C_j$: $V_{ij} = |\hat C_i \cap \mathbf C_j|$.
The NMI provides values between 0 (no overlap between clusters) and 1 (perfect clustering).


\subsection{Selection of Clustering Results to Fuse}
\label{sec:algorithms_and_metrics}
To have a wide variety and a large number of clustering results, we employ several clustering algorithms using different parameters.
For the selection of clustering algorithms to fuse, we designed three rules.
First, the algorithms should be powered by different principles, for example being density-based, centroid-based, or connectivity-based.
Second, the algorithms should require different primary parameters, such as a distance threshold or the number of clusters.
Third and most importantly, the performance of the algorithms should be sufficient.
Based on our criteria, we selected four different algorithms: K-Means $A^K$ \cite{macqueen1967k-means}, DBSCAN $A^D$ \cite{ester1996desitybased}, agglomerative clustering $A^A$ \cite{kaufman:clustering1990}, and ECLIPSE $A^E$ \cite{ECLIPSE}.

Since the desired measures for a given dataset are unknown, we employ different distance and similarity measures inside these algorithms.
Particularly, we use three common distance metrics: Euclidean $M^{eu}$,  cityblock $M^{city}$, Canberra $M^{can}$, as well as two similarity measures: cosine $M^{cos}$ and Bray-Curtis $M^{bc}$. While less well known, Canberra and Bray-Curtis are often used in clustering applications \cite{bouguettaya2015efficient,aljohani2017comparison,papadakis2017use,bondi2017tampering}.

Finally, each of the algorithms is run with several values of their respective primary parameters, which is the distance threshold for $A^D$ and the number of clusters for $A^K$, $A^A$, and $A^E$.
The range for the number of clusters is defined by $[\max(V/100,2), \min(V/10,100)]$.
In this range, we select each possible $K$.
To define a range for the distance threshold for $A^D$, we introduce our novel distance threshold estimate $\tau_m$ for each measure:
\begin{equation}
  \label{eq:threshold}
  \tau_m = \frac5{V^2} \sum\limits_{i=1}^V \sum\limits_{j=1}^{\frac {V}{5}} D_{ij}
\end{equation}
where $D_i = \text{sort}(\{ d(x_i, x_j) \mid x_j \in X \setminus \{x_i\} \})$ are the sorted distances of point $x_i$ to all other points $x_j \in X$.
Now, the threshold range is defined as $[\tau_m/2, 2\tau_m]$ from which we sample 10 equally spaced thresholds.


%\subsection{Experimental Results}
%\subsubsection{ComFu on UCI Datasets}
\subsection{Experiment on UCI Datasets}
\begin{table*}[t!]
  \centering
  \small
%  \setlength{\tabcolsep}{.4em}
  \begin{tabular}{c||c|c|c|c|c||c|c|c|c}
      Algorithms        & $M^{cos}$ & $M^{eu}$ & $M^{bc}$ & $M^{can}$ & $M^{city}$ & $M_1$ & $M_2$ & $M_3$ & $M_4$ \\\hline
      $A^A$   & \FB{.858} & .806 & .723 & .697 & .778 & .695 & .723 & .715 & \FB{.761} \\
      $A^E$   & .847 & .789 & .761 & .761 & .722 & .709 & .709 & .709 & .709 \\
      $A^D$   & .719 & \LB{.470} & \LB{.470} & \LB{.470} & \LB{.470} & \LB{.640} & .742 & .742 & .742 \\
      $A^K$   & --- & .798 & --- & --- & --- & --- & --- & --- & --- \\\hline
      $A^{A+E}$ & .871 & .709 & .723 & .697 & .778 & .715 & .712 & .710 & .709 \\
      $A^{A+D}$ & .858 & .600 & .706 & \LB{.444} & .596 & \LB{.695} & .700 & .715 & .761 \\
      $A^{A+K}$ & \FB{.901} & .806 & .770 & .684 & .778 & .712 & .806 & .770 & .778 \\
      $A^{E+D}$ & .709 & .761 & .761 & .761 & .761 & .761 & \FB{.898} & .846 & .850 \\      $A^{E+K}$ & .712 & .709 & .717 & .761 & .709 & .709 & .709 & .709 & .709 \\

      $A^{D+K}$ & .798 & .761 & .761 & .761 & .761 & .798 & .761 & .761 & .761 \\\hline
      $A^{A+E+D}$ & .871 & .709 & .723 & .697 & .778 & .898 & \FB{.901} & .864 & .880 \\
      $A^{A+E+K}$ & .761 & .709 & .770 & .761 & .778 & .712 & \LB{.709} & .710 & \LB{.709} \\
      $A^{A+K+D}$ & .901 & .806 & .770 & \LB{.684} & .778 & .712 & .806 & .770 & .778 \\
      $A^{K+E+D}$ & \FB{.930} & .709 & .717 & .761 & .709 & .722 & .880 & .846 & .833 \\\hline
      $A^{A+E+D+K}$ & \FB{.901} & .709 & .770 & \LB{.761} & .778 & \FB{.880} & \LB{.822} & \FB{.880} & .857 \\

  \end{tabular}
  \capt{tab:exhaustive}{Ablation study of Fused Algorithms and Measures}{
    This table provides an excerpt of the ablation study performed  for combinations of fused base clustering algorithms, and combinations of measures used inside of these algorithms on the Iris dataset using $K=3$.
    The top-left part of the matrix shows NMI values for fusing single algorithms with single measures, while lower rows show the fusion of different algorithms, and the right columns show the fusion of different measures with $M_1 = M^{cos+eu}$, $M_2= M^{cos+eu+bc}$, $M_3 = M^{cos+eu+bc+can}$, $M_4 = M^{cos+eu+bc+can+city}$.
    The \FB{Highest} and {\LB{lowest}} values are marked per block.
    For $A^K$ (K-Means), SciPy supports only Euclidean distance $M^{eu}$.
  }
\end{table*}

Theoretically, we can select any combination of algorithms and measures.
However, the most suitable combination may vary between the different datasets.
In a real system using clustering as a preprocessing, it would be tuned to optimize that performance.
For the initial evaluation, since we are testing in a generic setting,  in keeping with past papers on clustering evaluation we presume we get feedback from an NMI oracle, which we use to select the best combination of algorithms and measures or to select the best base algorithm.
Partial results of different ComFu combinations, in terms of NMI, on the Iris dataset can be found in \tab{exhaustive}.
There, we compare the fusion of single algorithms with single measures and different values of their corresponding primary parameters, which are shown in the top-left of the table, with the fusion of several algorithms and several combinations of measures.
First, we notice that the performance differs dramatically between different algorithms or measures.
For example, $A^D$ performs far inferior to $A^A$, especially when used with a single measure different than $M^{cos}$.
The fusion $A^{A+D}$ can mitigate the issues with some of the measures but performs even worse when used only with the $M^{can}$ measure.
When fusing several algorithms and several measures, we can observe that the lowest NMI scores generally increase with increasing numbers of fused algorithms.
While the best fusion result on the Iris dataset is obtained by the combination of $A^{K+E+D}$ with the measure $M^{cos}$, the combination of all algorithms with all measures shown in the bottom-right cell of \tab{exhaustive} is still comparably high, comparable to the best single algorithm with its best measures and parameters.



\begin{table}[bth]
  \centering
  \small
%  \setlength{\tabcolsep}{.1em}
  \begin{tabular}{c|c|c|c}
    Dataset  & Algorithms & Measures & NMI \\\hline
    Breast Cancer & $A^{E+D+K}$ & $M^{can+eu}$ & .732 \\
    Digits  & $A^{A+D}$ & $M^{cos+eu}$ & .871 \\
    Glass & $A^{E+A+K+D}$ & $M^{cos+eu+bc+city}$ & .536 \\
    Ionosphere & $A^{E+A+K+D}$ & $M^{eu}$ & .534 \\
    Iris  & $A^{E+K+D}$ & $M^{cos}$ & .930 \\
    Seeds  & $A^{E+A+K}$ & $M^{cos+city}$ & .732 \\
    Wine  & $A^{E+A+D}$ & $M^{can+eu}$ & .851 \\
    Yeast & $A^{E+A+K}$ & $M^{cos+eu}$ & .327 \\
  \end{tabular}

  \capt{tab:combination}{Best Combinations for ComFu}{
    In this table, the best combinations of algorithms and measures used in ComFu are displayed for each of the eight evaluated UCI datasets.
  }
\end{table}

When performing a comparison of the measures on different datasets, no measure works well on every dataset, because the structure, features, and attributes vary drastically between the different datasets.
For example, $M^{cos}$ performs very well on the Iris dataset and $M^{can}$ is well-suited for the Wine dataset.
However, the reverse combination does not provide good results, i.e., $M^{can}$ does not work on Iris and $M^{cos}$ performs poorly on Wine.
\tab{combination} shows the best combination of algorithms and measures on the eight evaluated datasets.
As we can see, on the one hand, the combination of measures depends highly on the dataset, and for some datasets, a single measure performs best.
On the other hand, when looking at the combinations of algorithms, it is better to fuse results of more than one algorithm, throughout.
Also, while agglomerative clustering $A^A$ and ECLIPSE $A^E$ generally perform better than K-Means $A^K$ and DBSCAN $A^D$, the latter two algorithms seem to include complementary information that can be exploited when fusing via ComFu -- the algorithm combination for the different datasets always include at least one of $A^A$ or $A^E$, but also at least one of $A^K$ or $A^D$.
To obtain good fusion results, the rules of selection of algorithm combinations and measure combinations are:
(A) The measure combination should contain at least one measure that works well on the dataset.
(B) The algorithm combination should include at least one powerful clustering algorithm such as $A^E$ or $A^A$ and one complementary algorithm from $A^D$ or $A^K$.


\begin{table}[bth]
  \centering
  \small
  \setlength{\tabcolsep}{.4em}
  \begin{tabular}{c||c|c|c|c}
    Dataset & Single-O & ComFu-O & ComFu-E & ComFu-A \\
      \hline
    Breast Cancer & .533 & \FB{.732}  &  .600      & \SB{.646}      \\
    Digits & \SB{.798} & \FB{.871}         &  .782 & .787    \\
    Glass &  .440 & \FB{.536}          &  .375     & \SB{.467}    \\
    Ionosphere & \SB{.388} & \FB{.534}     &  .259 & .364    \\
    Iris & .858 & \FB{.931}           &  .880      & \SB{.901}    \\
    Seeds & .610 & \FB{.732}          &  \SB{.655} & .641    \\
    Wine & .776 & \FB{.851}           &  .518      & \SB{.812}    \\
    Yeast & .263 & \FB{.311}          &  \SB{.310} & .291           \\
  \end{tabular}
  \capt{tab:single}{ComFu Variants vs. Best Single Algorithm}{
    This table compares the best single optimized clustering algorithm (optimized over algorithm,  measure, and parameters)  against the best combination of algorithms and measures fused using optimized ComFu-O, in terms of NMI.  This shows that commonality fusion can significantly improve over the best single algorithm.  ComFu-E shows fusion over all algorithms/measures/parameters, showing the results optimized only over the number of clusters K.  ComFu-A shows fusion with commonality-based automatic measure selection over algorithms/parameters, again showing results for the best K.  The automatic measure selection in ComFu-A is superior to fusing all measures (ComFu-E) in 6 out of the 8 cases and is also better than the best single optimized algorithm in 6 out of 8 cases. The \FB{best} and \SB{second} best performances per dataset are highlighted.
  }

\end{table}



An essential factor of a clustering fusion algorithm is that the fused result at least performs as well as the best base algorithm that is fused; otherwise, the fusion of algorithms proves useless.
Therefore, we record highest scores of any single algorithm with the best parameters of that algorithm in combination with any single measure (without fusion).
The scores are obtained by the optimal number of clusters $K$ in $A^A$, $A^E$, and $A^K$, and an optimal distance threshold in DBSCAN.
For ComFu, the highest scores are obtained by the best number of clusters $K$ and the best combinations of algorithms and measures.
The improvements between single algorithm and fused algorithms are provided in \tab{single}.
In all eight datasets, we can see that ComFu can outperform the best single algorithm.
Particularly in the Ionosphere dataset, the NMI score of ComFu is 27.3\% better than the best single algorithm.
These results prove that we can provide significantly better results compared to single algorithms.


Although many applications can provide the feedback needed to optimize their clustering, many others cannot and even when they can, optimizing over 100s of combinations of measures and parameters may be too expensive to be practical.
Thus, we sought to develop a version of ComFu that requires only one parameter, the number of clusters $K$ that is also necessary for other clustering algorithms such as K-Means and agglomerative clustering.
The first of these approaches fuses all algorithms and all distance measures, using the range of automatically selected parameters discussed above.
This algorithm ComFu-E is effective compared to the Oracle-optimized versions, as we can see in \tab{single}.

%\fi

To show that ComFu advances the state-of-the-art with respect to other clustering fusion algorithms, \tab{sota}  compares ComFu with   WCT \cite{journals/pami/Iam-onBGP11}, two system from 2015:
GPMGLA \cite{journals/ijon/HuangLW15}, WEAC \cite{journals/ijon/HuangLW15}, and two new ones from 2018: ACE \cite{alqurashi2018clustering}, and WOCIL \cite{jia2018subspace}.  
These algorithms were described in \sec{related}. 
We use their optimized results as reported in those papers, for which each of them reported the state-of-the-art clustering results at least on one of the eight considered datasets. 

%We also add the results of the best single algorithm given by \tab{single} since these sometimes already outperform other algorithms.
%The results are graphically displayed in \fig{sota}.
%Since not every of the compared approaches provides NMI scores for all eight datasets that we selected in our experiment, we simply leave the associated columns empty.
%
%The results are given in \tab{sota}.
Overall, ComFu-O provides the best results on five datasets:  Digits, Ionosphere, Iris, Seeds, and Yeast.
For the other  datasets, i.e., Breast Cancer, Glass and Wine, ComFu-O provides the second or third best results.  
While these papers all present optimized result, such tuning is not always practical.  
Considering  ComFu-A (cf.~\sec{automatic}),  a fully automatic fusion, we see that ComFu-A outperforms prior work, i.e., ComFu-A provides state-of-the-art clustering fusion.

Interestingly, ComFu is the only algorithm that can obtain high results throughout all of the evaluated datasets.
For example, the WCT algorithm that slightly outperforms ComFu on Wine performs poorly on Breast Cancer, Glass and Ionosphere.
Similarly, the ACE algorithm that works best on Glass is ineffective on Ionosphere and Wine.
While the results of GPMGLA and WEAC are somewhat stable across the datasets, their performance is generally inferior to ComFu.
Hence, ComFu proves to be well-suited for many different datasets and very different tasks.

%\subsection{Comparison to the State of the Art}


\newcommand{\thead}[1]{\multicolumn{1}{l}{\rlap{\rotatebox{60}{#1}}}} 


%\iffalse
\begin{table}[t!]
  \centering
  \footnotesize
  \setlength{\tabcolsep}{.4em}
  \begin{tabular}{c||c|c|c|c|c|c||c}
    %               & \SS WCT   & \SS GPMGLA & \SS WEAC  & \SS ACE    & \SS WOCIL & \SS ComFu-O\\      \hline
    % Breast Cancer & .668     & .715       & .672     & \SB{.776}  & \FB{.809}  & \TB{.732}\\
    % Digits        & \TB{.765} & \SB{.791}   & .760     & ---        & .743      & \FB{.871}\\
    % Glass         & \TB{.388} & ---         & ---       & \FB{.726}  & ---        & \SB{.536}\\
    % Ionosphere    & \SB{.228} & ---         & ---       & .123      & .143      & \FB{.534}\\
    % Iris          & .724     & .733       &  .731    & \TB{.766}  & \SB{.806}     & \FB{.931}\\
    % Seeds         & .573     & \TB{.595}   & \SB{.604} & ---        & ---        & \FB{.732}\\
    % Wine          & \FB{.881} & .797       & .767     & .429      & \SB{.861}  & \TB{.851}\\
    % Yeast         & \SB{.268} & .254       & \TB{.256}     & ---        & ---        & \FB{.311}\\

\normalsize Dataset     & \thead{WCT}   &\thead{ GPMGLA} &\thead{ WEAC}  &\thead{ ACE}    &\thead{ WOCIL} &\thead{ ComFu-O} &\thead{ ComFu-A}  \\\hline 
    Breast Cancer & .668     & .715       & .672     & \SB{.776}  & \FB{.809}  & \TB{.732} & {.646}      \\
    Digits        & {.765} & \SB{.791}   & .760     & ---        & .743     & \FB{.871}  & \TB{.787}    \\       
    Glass         & {.388} & ---         & ---       & \FB{.726}  & ---     & \SB{.536}  & \TB{.467}    \\  
    Ionosphere    & \TB{.228} & ---         & ---       & .123      & .143     & \FB{.534}  & \SB{.364}    \\       
    Iris          & .724     & .733       &  .731    & {.766}  & \TB{.806}  & \FB{.931}  & \SB{.901}    \\  
    Seeds         & .573     & {.595}   & \TB{.604} & ---        & ---      & \FB{.732}  & \SB{.641}    \\       
    Wine          & \FB{.881} & .797       & .767     & .429      & \SB{.861}  & \TB{.851}  & {.812}    \\  
    Yeast         & \TB{.268} & .254       & {.256}     & ---        & ---  & \FB{.311}  & \SB{.291}           \\


  \end{tabular}
  \capt{tab:sota}{Comparison to the State of the Art}{
     This table includes the performance of ComFu in comparison with other clustering fusion algorithms using their reported optimized NMI results on eight UCI datasets.     
     Algorithms that did not report on a given dataset are marked with ---. 
     Results to left of $||$ are optimized and each algorithm was state of the art on at least one dataset.  ComFu-O is \FB{best} on five, \SB{second best} on another and \TB{thrid} for the rest.  Fully automatic ComFu-A is better than the prior art on four datasets and second best on two others.
  }
  \end{table}



\subsection{Automatic Measure Combination Selection}
\label{sec:automatic}
One variation, denoted as ComFu-A, fuses all algorithms $A^{A+D+E+K}$ to take the different information from all clustering algorithms but introduce an automatic commonality-based selection of the distance measures. 
Since the five distance measures we selected generally do not provide enough informative difference among four algorithms on all datasets, we need to choose only a few. 
The idea of automatic selection is that we execute a selected single algorithm with all the distance measures by using a fixed threshold and evaluate the commonality of all distance measures.
To do this, we compute the commonality among the distance measures for the given single algorithm.
We sum the commonality score for each pair of points, and we choose the distance measures that provides the lowest total commonality.
The intuition here is that the measure with the least commonality is the most independent and informative as the high-commonality points are likely shared across more algorithms/measures.
Combining independent sources provides more value for fusion than fusing highly correlated results.

For each of the N measures with clustering results $C^m$, the commonality matrix $\check Q(M_i, M_j)$ between the each pair of measures can be computed by summing the commonality between the points $x_a$ from $C_i$ and points $x_b$ from $C_j$ by using original commonality matrix $Q$: 
%Given $N$ distance measures, each result of measures $C^m$, 
\begin{equation}
  \label{eq:selection_commonality_matrix}
  \mathcal{Q}(M_i,M_j) = \sum_{x_a \in C^i} \sum_{x_b \in C^j} Q(x_a, x_b)
  %hat Q(M_i,M_j) = 
\end{equation} 
Then, the total commonality of each distance measures $\mathcal{\cal Q}(M_i) $ can be computed by summing the commonality of the distance measures $M_i$ to other measures $M_j$ of each pair $(M_i, M_j)$ where $M_i \in M$ and $M_j \in M$:
\begin{equation}
  \label{eq:selection_commonality}
  %\check Q(M_i,M_j) = \sum_{m=1}^{N} \sum_{n=1}^{N} Q(x_i, y_i \mid M^m,M^n)
  \mathcal{\check Q}(M_i) = \sum_{j=1}^{N} {\cal Q}(M_i, M_j)
\end{equation} 

%\begin{equation}
%  \label{eq:selection_commonality}
  
  %\check Q(C_i,C_j) = \sum_{m=1}^{M} \bar Q(C_i,C_j \mid C^m)\,.
%\end{equation}
%\todo{This equation does not make sense. $\bar Q$ in \eqref{eq:fused-sim} does not have an extra parameter $\mid C^m$. I am not sure, what exactly you are computing here.}
%As seen in \tab{single}, using this automatically selected combination of measures is generally closer to the optimal fusion than ComFu-E, which just fuses all distance measures. 




\subsection{Experiment on IARPA Janus Benchmark-B}
The IJB-B dataset \cite{whitelam2017ijbb} is a novel facial image dataset with a total of 68195 images of 1845 subjects.
IJB-B contains images of celebrities in very uncontrolled conditions, reaching from frontal to over-full-profile images, uncontrolled illumination, very low resolution, and difficult occlusions. 
The clustering protocols are split into seven different sub-protocols with increasing number of subjects listed in the header of \tab{ijb-b-comfu}. 



\begin{table}[t!]
  \centering
  \footnotesize
  \setlength{\tabcolsep}{.5em}
  \begin{tabular}{c||c|c|c|c|c|c|c}
     Subjects & 32 & 64 & 128 & 256 & 512 & 1024 & 1845 \\
     Images   & 1026 & 2080 & 5224 & 9867 & 18251 & 36575 & 68195\\\hline
%      & NMI & NMI & NMI & NMI & NMI & NMI & NMI \\\hline
     SSC-OMP \cite{You2016ScalableSS}  & .575 & .539 & .476& .483 & .521& --- & ---\\
     PAHC \cite{Lin2017APH}  & .891& .898& .863 & .865 & .882& .890& .890\\ \hline
     K-Means \cite{macqueen1967k-means}  & .806 & .837 & .835 & .838 & .839 & .851 & .854\\
     AP \cite{Frey2007ClusteringBP}  & .814& .831 & .822& .836& .847& .854 & .858\\
     DBSCAN  \cite{ester1996desitybased} & .896& .885 & .893 & .895& .888& .894 & .895\\
     AHC \cite{Gowda1978AgglomerativeCU}  & .915& .912 & .925& .922 & .918& .919& .921\\
     DDC-NEG \cite{Dong_2018_CVPR}  & .919 & .915 & .927& .926& .918& .922 & .925\\
    \bf ComFu-A  &\FB{.975} &\FB{.941} &\FB{.954}&\FB{.956}&\FB{.950}&\FB{.950}&\FB{.951}\\
  \end{tabular}
  \capt{tab:ijb-b-comfu}{ComFu vs. Single Clustering Algorithms on IJB-B}{
     This table show the NMI scores by comparing the single clustering algorithms including the state of the art (DDC-NEG) on the seven IJB-B clustering protocols. 
     The first two (SSC-COP and PACH) use different features, the rest were reported on DC-NEG features. 
     Algorithms that did not report results on a dataset are marked with ---.
     \FB{Best} results are highlighted.}
\end{table}


We compare ComFu to the state-of-the-art single clustering approach from CVPR 2018: DDC-NEG \cite{Dong_2018_CVPR} as well as to other significant clustering algorithms on IJB-B to prove our ensemble approach could obtain significant results over other single clustering algorithms. 
To facilitate direct comparison, we use deep features provided by \cite{Dong_2018_CVPR}.

In this case, we select algorithm combination as $A^{A+D+E}$ for ComFu.
Note that this metric combination is automatic generated as $M^{cos+eu}$ for this dataset, as also related research optimizes their algorithms on the IJB-B clustering test set.
From \tab{ijb-b-comfu}, we can see that ComFu has a significantly better NMI score than previous algorithms, including the state-of-the-art approach DDC-NEG on all seven protocols.   
ComFu achieves a 0.975 NMI score on the protocol 32, which is much better than the DDC-NEG's 0.919. 
For the large protocols such as 512, 1024, and 1845, which contain a large number of images, ComFu obtains NMI scores around 0 .95,  significantly better than the DDC-NEG's NMI scores around 0.92. 
Though IJB-B contains a large number of images, NMI scores of ComFu on IJB-B protocols are much higher than on the UCI datasets.
We attribute this artifact to the use of the deep features of DDC-NEG, which perform much better than the hand-crafted features in the UCI datasets. 
This also shows that the datasets of UCI are not easy for clustering, even though they are small.

\subsection{ComFu Improves a Deep Learning System}
To show that our method is able to perform provide better preprocessing to improve system performance, we apply ComFu in the preprocessing stage of a state-of-the-art deep learning system: style aggregated network (SAN) for facial landmark detection \cite{Dong_2018_CVPR}, using use their code and data. 
The training/testing is done on the 300-W dataset \cite{Sagonas2013300FI}, which annotates five face datasets with 68 landmarks. 
The set test contains 689 images combining 554 common and 135 challenging subsets. 

\begin{figure}[t]
  \includegraphics[width=\columnwidth]{figures/ced.png}
  \capt{fig:ced}{Cumulative Error Distribution Curve for SAN on 300-W Dataset}{
    Normalized mean error (NME) is applied to evaluate the performance of SAN.  
    The blue line shows the cumulative error performance of original SAN with K-Means; the red line shows the cumulative error after replacing K-Means with ComFu-A. 
    ComFu reduces larger errors, and is statistically significantly better. }
\end{figure}


SAN is a style-aggregated method for facial landmark detection.
It uses clustering to address the large intrinsic variance of image styles.  
The authors of \cite{Dong_2018_CVPR} train the style-aggregated face generation module by calling a clustering algorithm to group faces using their style-discriminative features, and show that such clustered training improves accuracy.   
The original SAN system uses K-Means clustering.
In this work, we use their code directly and simply replace the call to K-Means with ComFu-A. %\todo{Add the information, which algorithms and measures were used in these experiments. If ComFu-A was used, please replace ComFu with ComFu-A throughout this subsection.}

To reduce interaction effects, we follow their evaluation using the ground truth face bounding box, marked as $SAN^{GT}$ in their paper.  
Looking at  the cumulative error distribution (CED) curve in \fig{ced}, we can observe that SAN with ComFu performs  similarly for smaller  normalized mean errors (NME).
When NME becomes larger than 0.06, the cumulative error of SAN with ComFu is always lower than SAN with K-Means, i.e., we reduce the frequency of larger errors.
A paired t-test shows that using ComFu statistically significantly reduces their error ($p<.0001$), so we advance the state of the art by simply replacing the clustering technique.
This result shows that ComFu is also useful as a component in a state-of-the-art the deep learning system, where clustering is a part of the preprocessing.

