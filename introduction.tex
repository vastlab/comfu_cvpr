\section{Introduction}
\label{sec:introduction}


\begin{figure}[t]
\includegraphics[width=\columnwidth]{teaser.pdf} \capt{fig:teaser}{Structure of Commonality Clustering Fusion}
{
ComFu improves clustering by fusing different clustering results balancing the goal of commonality with noise reduction.
Given the different base clustering outputs, we compute a commonality matrix, by counting how often each pair of points in the dataset co-occur, summing over each of the clustering algorithms. 
We build initial clusters from points with high commonality which are reliable, then assign the remaining noisier points to maximize their commonality but not impact clustering.
} \end{figure}



Clustering, the unsupervised grouping of data, is a well-studied problem in its own right, as well as a common element in vision algorithms and systems -- the proceedings of CVPR/ECCV 2018 include more than 400 papers focused on clustering or using clustering in preprocessing.
Clustering can be the direct result/goal
\cite{lin2018deep,zhou2018deep,you2018scalable}, it can be used as a algorithm for unsupervised or semi-supervised learning \cite{caron2018deep,zhao2018learning}, or as a component in a more complex vision algorithm \cite{Dong_2018_CVPR,Qi_2018_CVPR,Ehsani_2018_CVPR,Park_2018_CVPR}.
Formalizing clustering can be tricky because there are infinitely many different measures of distance/similarity and a plethora of clustering approaches.
Each combination yields  an ``optimal" set of clusters under different assumptions.

So what is a researcher to do?
An ideal solution would be an algorithm that would automatically incorporate the advantages of multiple algorithms.
To overcome the weaknesses of separate clustering algorithms, clustering fusion seeks to combine multiple algorithms/parameters/measures and come to a consensus. 
%In \sec{related}, we review the state-of-the-art clustering fusion algorithms.


This paper has five primary contributions:
\begin{enumerate}[nosep]
\item The novel commonality fusion (ComFu) algorithm, summarized in \fig{teaser}, which fuses over algorithms, similarity measures, and parameters.


\item A novel commonality-based selection criterion to automatically select among measures/parameters.


\item A comparison with five recent state-of-the-art clustering fusion algorithms using eight UCI datasets showing that ComFu outperforms them on five out of eight datasets and provides second-best performance on another, and always in the top 3 algorithms.


\item ComFu applies in real-world problems using high-dimensional deep features and 10,000s of samples.
In particular, we apply it to face identity clustering on the IJB-B dataset \cite{whitelam2017ijbb}.
Using the same deep features as in Lin et al.'s CVPR18 paper \cite{lin2018deep}, we show ComFu significantly improves their results, thereby advancing the state-of-the-art performance on identity clustering.


\item Finally, we show that ComFu can be used as a drop-in replacement in vision systems.
We adapt code from \cite{Dong_2018_CVPR},  a CVPR 2018 system to improve facial landmark detection, which uses K-means in the preprocessing for image style clustering.
We show that, by clustering with ComFu, their system has statistically significantly lower error, and  ComFu further advances their state-of-the-art performance.

\end{enumerate}
