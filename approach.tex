\section{Approach}
\label{sec:approach}


As typical for clustering algorithms, commonality fusion (ComFu) requires one parameter to be specified, which in our case is the final number of desired clusters $K$.
Based on the categories of \cite{journals/corr/HuangLW14b}, ComFu is a pairwise commonality-based ensemble clustering method.
Before we formalize our approach, we provide some intuition.
The core insight for ComFu is that two points are more likely to be in the same "correct" cluster if they have high commonality.
On the other hand, we do not want to maximize overall co-occurrence because the pairings for low commonality points are too noisy.
To understand that a simple commonality maximization is bad, note that one obtains maximum commonality by fusing algorithms where the parameters produce only one cluster -- probably not the best result.
Thus, we have to balance commonality with other factors.
Even a given fixed set of algorithms and parameters, we have found that the lower-commonality points are erratic while only the high-commonality points cluster stably.
Though the intuition may be helpful, this requires defining ``high" and ``low" thresholds, which is problematic.
Instead, ComFu uses high-commonality points to produce an over-segmentation to twice the desired number of clusters, which implicitly defines the high threshold.
It then performs cluster-merging to reduce the number of clusters to the desired level.
Finally, we compute a reweighted commonality measure and assign the remaining points based on it.

Using this technique, ComFu provides high commonality without maximizing pairwise commonality.
It avoids the noise of the low-commonality points impacting the core cluster formation while effectively automatically selecting thresholds for high/low.
Thus, ComFu is different from traditional majority voting or simple pairwise co-occurrence based algorithms such as \cite{journals/pami/FredJ04,conf/pakdd/LiYHL07,journals/pr/WangYZ09}.
Because ComFu weights the high-commonality points differently than low-commonality points, it is closer in spirit, but entirely different in details and performance, to weighted evidence accumulation clustering WEAC \cite{journals/ijon/HuangLW15} or WOCIL+ \cite{jia2018subspace}. 


\subsection{Pairwise Commonality}
Before introducing ComFu in detail, let us give a proper mathematic definition of the clustering fusion problem.
Assume a set $X=\{x_1, \ldots, x_V\}$ of samples to be clustered, with a total of $V$ samples.
For a clustering result $C$, we have a total of $N$ clusters:
\begin{equation}
  C = \{C_1,\ldots,C_N\}
\end{equation}
that contain all the points of $X$.
For a valid clustering result, two conditions need to be fulfilled: no sample is in two clusters and each sample is assigned to a cluster:
\begin{equation}
  \forall n_1 \neq n_2\colon C_{n_1} \cap C_{n_2} = \emptyset\quad\wedge\quad \bigcup_n C_n = X
\end{equation}
Let the number of samples in each cluster be $V_n=|C_n|$, so that $\sum_{n=1}^{N} V_n = V$.
For a given clustering result $C$, we define the relationship $R(x_i,x_j)$ of a pair of points $(x_i, x_j)$ as 1 if they belong to the same cluster and 0 if they belong to different clusters:
\begin{equation}
    \!\!\!R(x_i,x_j \mid C)\!=\!
\begin{cases}
    1\!\! &\! \exists n: x_i \in C_n \wedge x_j \in C_n; x_i \neq x_j\!\!\\
    0\!\! &\! \text{otherwise}
\end{cases}
\end{equation}

To fuse clustering results, presume a set $\mathcal S$ of cardinality $M$ of base clustering results:
\begin{equation}
    \mathcal{S} = \{C^1,\ldots,C^M\}
\end{equation}
where each clustering result $C^m$ can contain a different number of clusters $N^m$.
We define the commonality matrix $Q(x_i, x_j)$ between each pair of data samples $(x_i, x_j$) by calculating in how many clustering results the two samples are related, i.e., appear in the same cluster:
\begin{equation}
  \label{eq:similarity}
  Q(x_i,x_j) = \sum_{m=1}^{M} w^mR(x_i,x_j \mid C^m)
\end{equation}
where $w^m$ is the weight for each input clustering result $C^m$.
Weights add free parameters and -- if optimized on the test data as done in many related works -- improve performance.
To properly selected weights, one needs sufficient validation data to avoid overfitting.
Given such data, if one algorithm is noticeably worse, you could reduce its weight.
Since in our experiments we do not have validation data, throughout this paper we set  $w^m=1\,\forall m$.



\subsection{Initial Clusters}
Based on the commonality matrix $Q$ in \eqref{eq:similarity}, we develop our commonality fusion (ComFu) algorithm to build a valid clustering result.
There are two steps in ComFu.
First, we generate a large number of initial groups $\bar C$ by using pairs of points that have a strong mutual commonality, while leaving out points with lower commonality.
These initial groups of high commonality are joined to build our initial clusters $\hat C$.
Second, we assign the remaining points to the initial clusters, without building additional new clusters.

Initial groups define the core of the dataset that we can trust because they are built from high commonality pairs.
To be precise, we construct $\bar N$ initial groups $\bar C_n, n=1,\ldots,\bar N$ of highly connected points, where $\bar N$ should be larger than the desired number of clusters $K$.
In this paper, we choose $\bar N = 2K$, throughout.
We generate initial groups $\bar C = \{\bar C_1,\ldots, \bar C_{\bar N}\}$ from the strong pairs using the commonality matrix $Q$.
One initial group $\bar C_n$ is defined as:
\begin{equation}
  \bar C_n = \{x_{i_1}, x_{i_2},\ldots,x_{i_{V_n}} \mid \forall i_n,i_{n'} : Q(i_n,i_{n'}) \geq \theta \}
\end{equation}
where $\frac M2\leq\theta\leq M$ is initialized to the maximum commonality between each two points:
\begin{equation}
    \label{eq:max-sim}
    \theta = \max_{x_i, x_j \in X} Q(x_i, x_j).
\end{equation}
As needed $\theta$ may be reduced.
When the commonality $Q(x_{i_1}, x_{i_2})$ between points $x_i$ and $x_j$ is at least $\theta$, $x_i$ and $x_j$ are grouped together in one of our initial groups $\bar C$.
When several nodes are connected with at least commonality $\theta$, initial groups can be larger than only two points.
As long as the number of initial groups $|\bar C|$ is less than $\bar N$ and $\theta>\frac M2$, we reduce the $\theta$ by 1 and iterate the selection algorithm.

After generating the initial groups $\bar C$, we merge them into $K$ initial clusters $\hat C = \{\hat C_1,\ldots,\hat C_K\}$.
The cluster-wise commonality between two clusters $\bar C_{n_1}$, $\bar C_{n_2}$ is defined as:
\begin{equation}
  \label{eq:fused-sim}
  \bar Q(\bar C_{n_1}, \bar C_{n_2}) = \max_{x_i \in \bar C_{n_1}, x_j \in \bar C_{n_2}} Q(x_i, x_j)
\end{equation}
Similar to the threshold defined in \eqref{eq:max-sim}, we initialize the threshold $\bar \theta$ to the maximum group commonality:
\begin{equation}
    \label{eq:max-fused-sim}
    \bar \theta = \max_{n_1, n_2} \bar Q(\bar C_{n_1}, \bar C_{n_2})
\end{equation}
For all pairs of groups $(\bar C_{n_1},\bar C_{n_2})$, we merge groups to one cluster $\hat C_n$ if the maximum commonality of $\bar Q(\bar C_{n_1},\bar C_{n_2})$ is at least $\bar \theta$.
As before, while the desired number of clusters $K$ is not reached, $\bar \theta$ is decreased by 1 and the group fusion is iterated.
After the iterations finish, the initial clusters $\hat C$ are:
\begin{equation}
  \hat C = \{\hat C_1, \ldots, \hat C_K\}
\end{equation}

\subsection{Assigning Points with Low Commonality}
After building the initial clusters $\hat C$, we need to assign the remaining points with low commonality $\dot X$ that have not yet been assigned to an initial cluster:
\begin{equation}
  \dot X = \{x\mid x \in X \wedge \forall n \colon x \notin \hat C_n\}\,.
\end{equation}
For each point $x_i \in \dot X$, we need to select one of the initial clusters $\hat C$ to which we assign $x_i$.
The assignment also depends on our commonality matrix $Q$ in \eqref{eq:similarity}, which we use to compute the reweighted commonality $\hat Q(x_i, \hat C_n)$ between a point $x_i \in \dot X$ and an initial cluster $\hat C_n$:
\begin{equation}
  \hat Q(x_i, \hat C_n) = \sum_{x_j \in \hat C_n} \frac{Q(x_i, x_j)} {\hat V_n}
\end{equation}
where $\hat V_n = |\hat C_n|$ represents the number of samples in the initial cluster $\hat C_n$. The reweighting is accounting for the fact that larger clusters tend to absorb more low commonality points. 
We calculate the reweighted commonality of each remaining point $x_i$ to every initial cluster $\hat C_n$.
The point $x_i$ is assigned to the cluster $\hat C_n$ with the highest reweighted commonality:
\begin{equation}
  \argmax_n \hat Q(x_i, \hat C_n) \rightarrow x_i \in \hat C_n\,.
\end{equation}



