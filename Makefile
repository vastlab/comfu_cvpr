once:
	pdflatex -interaction=nonstopmode -synctex=1 paper.tex

all: once
	bibtex paper
	pdflatex -interaction=nonstopmode -synctex=1 paper.tex
	bibtex paper
	pdflatex -interaction=nonstopmode -synctex=1 paper.tex

clean:
	rm paper.pdf *.aux *.bbl *.blg *.log
